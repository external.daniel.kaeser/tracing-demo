# Distributed Tracing Demo

Wieso überhaupt?

Thema `Distributed Services Observability` [u.a. hier beschrieben](https://www.oreilly.com/library/view/distributed-systems-observability/9781492033431/ch04.html)

- Logging
- Metrics
- Tracing

Tracing fehlt uns noch.

## Kaffee UseCase vorstellen

![alt text](usecase.jpg "Usecase Kaffee")

## Beispiel starten

### Jaeger und RabbitMQ starten

```shell
docker-compose up -d 
```

- Danach Jaeger auf http://localhost:16686
- RabbitMQ auf http://localhost:15672 erreichbar (WebUI Credentials `guest`:`guest`

### Alle Services hier im Projekt starten

- ThekeApplication
- BaristaApplication
- KellnerApplication

## Kaffee bestellen

```shell
curl -i -X POST -H 'Content-Type: application/json' -d '{"bestellung": "latte", "tisch": 3}' localhost:9010/kaffee-bestellen
```

Hinweis:

Wenn man vor einer Bestellung den Kellner ausschaltet, kann man im RabbitMQ auf den Queues "get message" o.ä. machen und
man sieht in den Message Headers den `b3` Header mit ungefähr diesem
Format: `<traceid>-<spanid>-<flag, ob an jaeger gesendet wird>`.

Danach kann man den Kellner wieder anschalten. Danach sieht man im Jaeger z.B. auch die Gesamtzeit von Bestellung bis
Kellner-wiederhochfahren.

## Diskussion

- Jaeger zeigen

- Trace, Span
    - Sampling Rate

- Sleuth, Zipkin => Jaeger oder auch AWS X-Ray

- Developer UseCases
    - Tracing, Latenzen sehen
    - z.B. Tests: S&R Requests injecten TraceId

- Kibana, Jaeger Custom Links

- SNS+SQS Instrumentation
    - Mal ein Beispiel in der freien Wildbahn machen QuimusClient + ArtikelService + AuftraegeService +
      Bibliografieanalyzer

## Hauptsächlich geklaut von

- https://docs.spring.io/spring-cloud-sleuth/docs/3.0.1/reference/htmlsingle
- https://spring.io/projects/spring-cloud-sleuth
- https://github.com/openzipkin-contrib/brave-opentracing
- https://zipkin.io/
- https://www.jaegertracing.io
