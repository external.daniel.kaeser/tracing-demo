package de.libri.tracing.demo.barista;

import brave.ScopedSpan;
import brave.Tracer;
import de.libri.tracing.demo.common.Kaffee;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import static de.libri.tracing.demo.common.KaffeeConstants.TOPIC_EXCHANGE_NAME;

@Slf4j
@RestController
@SpringBootApplication
@RequiredArgsConstructor
public class BaristaApplication {

    private final BaristaService baristaService;

    public static void main(final String[] args) {
        SpringApplication.run(BaristaApplication.class, args);
    }

    @PostMapping("kaffee-zubereiten")
    public ResponseEntity<?> kaffeeZubereiten(@RequestBody final Kaffee kaffee) {
        return ResponseEntity.ok(baristaService.zubereiten(kaffee));
    }

    @Component
    @RequiredArgsConstructor
    public static class BaristaService {

        private final Tracer tracer;
        private final RabbitTemplate rabbitTemplate;

        @SneakyThrows
        @NewSpan("zubereitung")
        public String zubereiten(final Kaffee kaffee) {
            if (kaffee.getBestellung().contains("macchiato")) {
                log.error("pfui, mach ich nicht");
                throw new IllegalArgumentException("igitt");
            }

            log.info("Ich bereite jetzt einen Kaffee zu: {}", kaffee);

            final ScopedSpan span = tracer.startScopedSpan("milch aufschäumen");
            try {
                span.tag("arbeitsschritt", "Milch aufschäumen");
                log.info("Zisch blubber...");
                span.annotate("blubb blubb schäum");
            } finally {
                span.finish();
            }

            log.info("Jetzt Kaffee eingießen...");

            rabbitTemplate.convertAndSend(TOPIC_EXCHANGE_NAME, "kaffee.whatever", kaffee);

            return String.format("hmm lecker %s", kaffee.getBestellung());
        }
    }

}
