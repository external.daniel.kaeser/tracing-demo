package de.libri.tracing.demo.common;

import lombok.Data;

@Data
public class Kaffee {

    private String bestellung;
    private int tisch;

}
