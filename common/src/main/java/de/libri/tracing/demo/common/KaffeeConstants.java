package de.libri.tracing.demo.common;

public final class KaffeeConstants {

    public static final String TOPIC_EXCHANGE_NAME = "kaffee-exchange";
    public static final String QUEUE_NAME = "kaffee-queue";
    public static final String ROUTING_KEY = "kaffee.#";

}
