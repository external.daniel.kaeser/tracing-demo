package de.libri.tracing.demo.kellner;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RestController;

import static de.libri.tracing.demo.common.KaffeeConstants.QUEUE_NAME;

@Slf4j
@RestController
@SpringBootApplication
public class KellnerApplication {

    public static void main(final String[] args) {
        SpringApplication.run(KellnerApplication.class, args);
    }

    @Component
    public static class Receiver {

        @RabbitListener(queues = QUEUE_NAME)
        public void receiveMessage(final Message message) {
            log.info("Na dann liefere ich mal den Kaffe aus: {}", message);
        }
    }

}
