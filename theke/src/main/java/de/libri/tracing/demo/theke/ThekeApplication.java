package de.libri.tracing.demo.theke;

import de.libri.tracing.demo.common.Kaffee;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Slf4j
@RestController
@SpringBootApplication
@RequiredArgsConstructor
public class ThekeApplication {

    private final RestTemplateBuilder restTemplateBuilder;

    public static void main(final String[] args) {
        SpringApplication.run(ThekeApplication.class, args);
    }

    @PostMapping(value = "kaffee-bestellen", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> kaffeeBestellen(@RequestBody final Kaffee kaffee) {
        log.info("Ein Kaffee wurde bestellt: {}", kaffee);

        final ResponseEntity<String> kaffeeResponseEntity = restTemplate()
                .postForEntity("http://localhost:9020/kaffee-zubereiten/", kaffee, String.class);

        return ResponseEntity.ok(Map.of("kaffee unterwegs:", kaffeeResponseEntity.getBody()));
    }

    @Bean
    public RestTemplate restTemplate() {
        return restTemplateBuilder.build();
    }
}
